package com.appi.test;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import com.appi.deng.R;
import com.appi.deng.Render;
import com.appi.deng.entity.animation.AnimatedSprite;
import com.appi.deng.entity.connectors.CollisionConnector;
import com.appi.deng.entity.connectors.FixedConnector;
import com.appi.deng.entity.layer.ParallaxLayer;
import com.appi.deng.entity.layer.StaticBackgroundLayer;
import com.appi.deng.entity.sprite.*;

public class TestActivity extends Activity{


	private int mScore = 0;  // Score - crashed ufos
	private int mLives = 5; // Lives


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_render);

		final Resources resources = getResources();

		final Render render = (Render)findViewById(R.id.render);



		// ******************************* Add Static background to render ****************************//
		final StaticBackgroundLayer staticBackgroundLayer = new StaticBackgroundLayer(resources.getDrawable(R.drawable.background_layer_2), StaticBackgroundLayer.ScaleType.FILL_XY);
		render.addLayer(staticBackgroundLayer);

		// **********************************************************************************//


		// ***************************** Add scrolled background to render***************************//
		final ParallaxLayer parallaxLayer = new ParallaxLayer(resources.getDrawable(R.drawable.parallax_bg_3));
		parallaxLayer.setY(0);
		parallaxLayer.setSpeed(20);
		render.addLayer(parallaxLayer);
		// *********************************************************************************//


		// ****************************** Create unit, that follow touch position *************************//
		final TouchFallowSprite spaceShipSprite = new TouchFallowSprite(resources.getDrawable(R.drawable.space_ship));
		spaceShipSprite.setMaxSpeed(300f);
		// ************************************************************************************************//

		// ******************************* Create bullets emitter ****************************************//
		final BulletsEmitter bulletsEmitter = new BulletsEmitter(resources.getDrawable(R.drawable.bullet_fire));
		bulletsEmitter.setEmittionFrequency(10);// 10 bullets per second
		bulletsEmitter.startEmittion(true);
		bulletsEmitter.setEmittionSpeed(500f);
		// ***********************************************************************************************//


		// ************************* Connect bullet emitter and unit (now emitter will follow unit) and add to render  ************************//
		final FixedConnector shipAndBulletEmitterConnector = new FixedConnector(spaceShipSprite, bulletsEmitter, FixedConnector.Anchor.RIGHT);
		render.addLayer(shipAndBulletEmitterConnector);
		// *****************************************************************************************************************************//


		// *********** Добавляем юнита и излучатель пуль на рендер после добавления коннектора, чтобы позиция эмиттера просчитывалась до его отрисовки (иначе будет отставать на 1н кадр) ****//
		render.addLayer(spaceShipSprite);
		render.addLayer(bulletsEmitter);
		// ****************************************************************************************************************************//


		// ***************** Create enemy units (destroyable) emitter ***************************************************//
		final EnemyEmitter ufoEmitter = new EnemyEmitter(resources.getDrawable(R.drawable.ufo_sprite));
		ufoEmitter.setEmittionFrequency(1);
		ufoEmitter.startEmittion(true);
		ufoEmitter.setEmittionSpeed(100f);
		// **************************************************************************************************//



		// **************** Create enemy units (not destroyable) emitter ***************************************************//
		final EnemyEmitter asteroidsEmitter = new EnemyEmitter(resources.getDrawable(R.drawable.asteroid));
		asteroidsEmitter.setEmittionFrequency(0.3f);
		asteroidsEmitter.startEmittion(true);
		asteroidsEmitter.setEmittionSpeed(50f);
		// ***************************************************************************************************************//



		// *********************** create explosion animation sprite, will be added to render later ******************************//
		final AnimatedSprite explosionSprite = new AnimatedSprite(resources.getDrawable(R.drawable.explosion_animation));
		explosionSprite.setOneShot(true);
		// **********************************************************************************************************************//


		// *********************** create score and lives text sprite *************************//
		final TextSprite scoreAndLivesTextSprite = new TextSprite();
		scoreAndLivesTextSprite.setTextColor(Color.WHITE);
		scoreAndLivesTextSprite.setTextSize(20);
		scoreAndLivesTextSprite.x = 100;
		scoreAndLivesTextSprite.y = 20;
		scoreAndLivesTextSprite.setText("Scores : "+mScore+"     Lives : "+mLives);
		// ***********************************************************************************//


		final CollisionConnector bulletsVSufo = new CollisionConnector(bulletsEmitter, ufoEmitter);
		bulletsVSufo.setCollisionListener(new CollisionConnector.CollisionListener() {
			@Override
			public void onCollide(Sprite spriteA, Sprite spriteB, Emitter.Emission emissionA, Emitter.Emission emissionB) {

				emissionA.destroy = true;
			    emissionB.destroy = true;

				explosionSprite.x = emissionA.x;
				explosionSprite.y = emissionA.y;
				explosionSprite.start();

				mScore ++;
				scoreAndLivesTextSprite.setText("Scores : "+mScore+"     Lives : "+mLives);
			}
		});


		final CollisionConnector ufoVSme = new CollisionConnector(ufoEmitter, spaceShipSprite);
		ufoVSme.setCollisionListener(new CollisionConnector.CollisionListener() {
			@Override
			public void onCollide(Sprite spriteA, Sprite spriteB, Emitter.Emission emissionA, Emitter.Emission emissionB) {
				emissionA.destroy = true;
				explosionSprite.x = emissionA.x;
				explosionSprite.y = emissionA.y;
				explosionSprite.start();


				mLives --; // decrement lives
				scoreAndLivesTextSprite.setText("Scores : "+mScore+"     Lives : "+mLives);
			}
		});

		final CollisionConnector asteroidsVSme = new CollisionConnector(asteroidsEmitter, spaceShipSprite);
		asteroidsVSme.setCollisionListener(new CollisionConnector.CollisionListener() {
			@Override
			public void onCollide(Sprite spriteA, Sprite spriteB, Emitter.Emission emissionA, Emitter.Emission emissionB) {
				emissionA.destroy = true;
				explosionSprite.x = emissionA.x;
				explosionSprite.y = emissionA.y;
				explosionSprite.start();

				mLives --;// decrement lives
				scoreAndLivesTextSprite.setText("Scores : "+mScore+"     Lives : "+mLives);
			}
		});

		final CollisionConnector bulletsVSasteroids = new CollisionConnector(bulletsEmitter, asteroidsEmitter);
		bulletsVSasteroids.setCollisionListener(new CollisionConnector.CollisionListener() {
			@Override
			public void onCollide(Sprite spriteA, Sprite spriteB, Emitter.Emission emissionA, Emitter.Emission emissionB) {
				emissionA.destroy = true;
			}
		});



		render.addLayer(bulletsVSufo);
		render.addLayer(ufoVSme);
		render.addLayer(asteroidsVSme);
		render.addLayer(bulletsVSasteroids);



		render.addLayer(asteroidsEmitter);
		render.addLayer(ufoEmitter);






		render.addLayer(explosionSprite);







		render.addLayer(scoreAndLivesTextSprite);

		// ******************* add fps counter **************** //
		render.addLayer(new FPSCounter());
		// **************************************************** //

    }
}
