package com.appi.deng.physics;

import com.appi.deng.entity.sprite.Sprite;
import com.appi.shpx.body.Body;
import com.appi.shpx.body.RectBody;
import com.appi.shpx.math.Vec2;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 6/5/14
 * Time: 12:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class SpriteBody extends RectBody {


	private final Sprite mSprite;

	public SpriteBody(Sprite sprite) {

		super(sprite.width, sprite.height);
		mSprite = sprite;

	}

	public Sprite getSprite(){
		return mSprite;
	}


	@Override
	public void apply() {
		mSprite.x = position.x;
		mSprite.y = position.y;
		if (force.x != 0 && force.y != 0){
			mSprite.angle = (float)( Math.atan2(force.y , force.x) + Math.PI /2 );
		}
	}
}
