package com.appi.deng.physics;

import com.appi.shpx.body.Body;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 9/1/14
 * Time: 3:13 PM
 * To change this template use File | Settings | File Templates.
 */
public interface CollideDetectedListener {

	public void onCollided(Body body, int tileId, float x, float y, int ricoshet);
	public void onCollided(Body bullet, Body target);
}
