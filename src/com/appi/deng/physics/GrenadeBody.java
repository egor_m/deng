package com.appi.deng.physics;

import com.appi.deng.entity.sprite.GrenadeFactory;
import com.appi.deng.entity.sprite.GrenadeSprite;
import com.appi.deng.interpolator.TriangleInterpolator;
import com.appi.shpx.body.Body;
import com.appi.shpx.level.Level;
import com.appi.shpx.math.Vec2;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 10/22/14
 * Time: 4:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class GrenadeBody extends Body{

	private final GrenadeSprite mGrenadesSprite;
	private float distanse;
	private final Vec2 mTmpNormilazedForce = new Vec2();
	private float mMaxDistance;

	private final GrenadeFactory mGrenadeFactory;

	private final float mGrenateWidth;
	private final float mGrenateHeight;

	private final TriangleInterpolator mTriangleInterpolator = new TriangleInterpolator();

	public GrenadeBody(GrenadeSprite grenadeSprite, GrenadeFactory grenadeFactory){
		mGrenadesSprite = grenadeSprite;
		mGrenateWidth = mGrenadesSprite.width;
		mGrenateHeight = mGrenadesSprite.height;

		mGrenadeFactory = grenadeFactory;
	}

	@Override
	public float sqrDistance(Vec2 point) {
		return 0;
	}

	public void fire(Vec2 position, float force, Vec2 to, float maxDistance){

		this.distanse = 0;

		mMaxDistance = Math.min(maxDistance, Vec2.distance(position, to));

		this.position.x = position.x;
		this.position.y = position.y;


		Vec2.nortalize(position, to, mTmpNormilazedForce);


		this.force.x = -mTmpNormilazedForce.x;
		this.force.y = -mTmpNormilazedForce.y;
		this.force.z = force;

		mGrenadesSprite.visible = true;

	}

	@Override
	public void applyDxMove(float dx, float dy, Level level, List<Body> objects) {
		super.applyDxMove(dx, dy, level, objects);

		distanse += Math.sqrt(Math.pow(dx,2)+ Math.pow(dy,2));
		if (distanse >= mMaxDistance){
			distanse = 0;
			mGrenadesSprite.explode();
			mGrenadeFactory.recycle(this);
		}
	}

	public void clear() {
		position.x = 0;
		position.y = 0;
		force.x = 0;
		force.y = 0;
		force.z = 0;

		mGrenadesSprite.clear();
	}

	@Override
	public void apply() {
		mGrenadesSprite.x = position.x;
		mGrenadesSprite.y = position.y;

		final float pct = distanse/ mMaxDistance;
		final float scale = mTriangleInterpolator.interpolate(pct);

		mGrenadesSprite.scale(scale + 1);
	}

	public GrenadeSprite getGrenadesSprite() {
		return mGrenadesSprite;
	}
}
