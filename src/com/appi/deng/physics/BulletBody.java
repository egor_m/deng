package com.appi.deng.physics;

import com.appi.deng.entity.Entity;
import com.appi.deng.entity.sprite.BulletFactory;
import com.appi.deng.entity.sprite.BulletSprite;
import com.appi.shpx.body.Body;
import com.appi.shpx.level.Level;
import com.appi.shpx.level.LevelObject;
import com.appi.shpx.math.Vec2;
import com.appi.shpx.math.Vec3;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 7/11/14
 * Time: 10:23 AM
 * To change this template use File | Settings | File Templates.
 */
public class BulletBody extends Body {



	private static final int WALL_VERTICAl = 1;
	private static final int WALL_HORIZONTAL = 2;
	private static final int WALL_CORNER = 3;


	private final BulletFactory mBulletFactory;
	private final BulletSprite mBulletSprite;
	public float distanse = 0;
	public float maxDistanse = 500;
	private Entity bulletSprite;

	private final Vec2 mTmpPosition = new Vec2();

	private CollideDetectedListener mCollideDetectedListener;

	public BulletBody(BulletSprite bulletSprite, BulletFactory bulletFactory){
		mBulletSprite = bulletSprite;
		mBulletFactory = bulletFactory;
		collidable = false;
		isBullet = true;
	}

	@Override
	public float sqrDistance(Vec2 point) {
		return 0;
	}

	@Override
	public void applyDxMove(float dx, float dy, Level level, List<Body> objects) {


		final float x = position.x + dx;
		final float y = position.y + dy;

		for (Body body : objects){
			if (body.isBullet) continue;
			if (! body.collidable) continue;
			if (body.collisionGroup == this.collisionGroup) continue;

			mTmpPosition.x = x;
			mTmpPosition.y = y;
			final float dst = body.sqrDistance(mTmpPosition);

			if (dst < body.sqrRadius){
				onCollidedWithSprite(this, body);
			}

		}


		final LevelObject levelObject = level.getLevelObject();

		final int oldCol =  (int) (position.x / levelObject.tilewidth);
		final int oldRow =  (int) (position.y / levelObject.tileheight);



		final int col = (int) ((x) / levelObject.tilewidth);
		final int row = (int) ((y) / levelObject.tileheight);


		int ricochet = WALL_CORNER;
		if (oldCol == col){
			ricochet = ricochet = WALL_VERTICAl;
		}else
		if (oldRow == row){
			ricochet = WALL_HORIZONTAL;
		}



		if (col < 0 || row < 0 || col > levelObject.width || row > levelObject.height){
			onCollidedWithWall(-1, x, y, ricochet); // TODO tile ID is faked
			return ;
		}

		final int tile = row * levelObject.width + col;

		try{  // TODO (CRITICAL) make other test !!!!
			if (levelObject.layers[0].data[tile] != 0){
				onCollidedWithWall(tile, x, y, ricochet);
				return ;
			}
		}catch (ArrayIndexOutOfBoundsException e){
			return ;
		}

		super.applyDxMove(dx, dy, level, objects);
	}

	public BulletSprite getBulletSprite() {
		return mBulletSprite;
	}



	public void registerCollideDetector(CollideDetectedListener collideDetectedListener){
		mCollideDetectedListener = collideDetectedListener;
	}


	public void clear(){
		position.x = 0;
		position.y = 0;
		force.x = 0;
		force.y = 0;
		force.z = 0;

		mBulletSprite.clear();
	}


	public void fire(Vec2 position, Vec3 force){

		this.distanse = 0;

		this.position.x = position.x;
		this.position.y = position.y;

		this.force.x = force.x;
		this.force.y = force.y;
		this.force.z = force.z;

		mBulletSprite.visible = true;

	}


	@Override
	public void apply() {
		mBulletSprite.x = position.x;
		mBulletSprite.y = position.y;
		if (force.x != 0 && force.y != 0){
			mBulletSprite.angle = (float)( Math.atan2(force.y , force.x) + Math.PI /2 );
		}
	}

	private void onCollidedWithWall(int tileId, float x, float y, int ricochet){
		if (ricochet == WALL_HORIZONTAL){
			force.x = -force.x;
		}else
		if (ricochet == WALL_VERTICAl){
			force.y = -force.y;
		}else{
			force.x = -force.x;
			force.y = - force.y;
		}





		mBulletFactory.recycle(this);


		final CollideDetectedListener collideDetectedListener = mCollideDetectedListener;
		if (collideDetectedListener != null){
			collideDetectedListener.onCollided(this, tileId, x, y, ricochet);
		}



	}


	private void onCollidedWithSprite(BulletBody bulletBody, Body target){
		final CollideDetectedListener collideDetectedListener = mCollideDetectedListener;
		if (collideDetectedListener != null){
			collideDetectedListener.onCollided(bulletBody , target);
		}
		mBulletFactory.recycle(this);
	}





}
