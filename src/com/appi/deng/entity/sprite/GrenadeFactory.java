package com.appi.deng.entity.sprite;

import android.graphics.drawable.Drawable;
import com.appi.deng.entity.layer.Layer;
import com.appi.deng.physics.CollideDetectedListener;
import com.appi.deng.physics.GrenadeBody;
import com.appi.shpx.World;

import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 10/22/14
 * Time: 4:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class GrenadeFactory {

	private final LinkedList<GrenadeBody> mGrenades = new LinkedList<GrenadeBody>();


	private GrenadeFactory(){
	}

	public static GrenadeFactory createFactory(int grenadesCount, Drawable grenadesDrawable, Drawable grenateExplosionAnimation, CollideDetectedListener collideDetectedListener){

		final GrenadeFactory grenadesFactory = new GrenadeFactory();

		for (int i = 0 ; i < grenadesCount; i++){

			final GrenadeSprite grenadesSprite = new GrenadeSprite(grenadesDrawable, grenateExplosionAnimation);
			grenadesSprite.visible = false;

			final GrenadeBody grenadesBody = new GrenadeBody(grenadesSprite, grenadesFactory);


			grenadesFactory.mGrenades.add(grenadesBody);
		}

		return grenadesFactory;

	}


	public synchronized GrenadeBody getFreeGrenades(long collisionGroup){
		if (mGrenades.isEmpty()) return null;
		final GrenadeBody bulletBody = mGrenades.pop();
		bulletBody.collisionGroup = collisionGroup;
		return bulletBody;
	}



	public synchronized void recycle(GrenadeBody grenadesBody){
		grenadesBody.clear();


		mGrenades.add(grenadesBody);
	}


	public void init(World world, Layer layer){
		for (GrenadeBody body : mGrenades){
			world.addBody(body);
			layer.addEntity(body.getGrenadesSprite());
		}
	}

}
