package com.appi.deng.entity.sprite;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.MotionEvent;
import com.appi.deng.TouchEventConverter;
import com.appi.deng.entity.Entity;
import com.appi.deng.entity.Touchable;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 12/25/13
 * Time: 5:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class FireControl extends Entity implements Touchable{


    public interface FireEventFistener{
        public void onFire();
    }


    public float reloadTime;
    private FireEventFistener mFireEventListener;


    private final float RADIUS = 100;

    private final Paint mPaint;


    public FireControl() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(5);
        mPaint.setColor(Color.RED);
    }

    @Override
    public void draw(Canvas canvas, float tfp) {

        final RectF viewPort = getViewPort();
        final float x = viewPort.right - RADIUS - 10;
        final float y = viewPort.bottom - RADIUS - 10;

        canvas.drawCircle(x,y,RADIUS, mPaint);

    }


    public void setFireEventListener(FireEventFistener fireEventListener){
        mFireEventListener = fireEventListener;
    }


    private boolean pressed = false;
    private int pointerId = -1;

    @Override
    public boolean onTouchEvent(MotionEvent event, TouchEventConverter converter) {
        final RectF viewPort = getViewPort();

        final int action = event.getActionMasked();

        int pointerIndex = event.getActionIndex();
        int pointerId = event.getPointerId(pointerIndex);


        final float x = converter.convertX(event.getX(pointerIndex));
        final float y = converter.convertY(event.getY(pointerIndex));

        final float TWO_RAIUS = RADIUS * 2;


            switch (action){
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_POINTER_DOWN:
                    pressed = true;
					if(this.pointerId == -1){
						this.pointerId = pointerId;
					} else{
						if (this.pointerId != pointerId) return false;
					}
                break;

				case MotionEvent.ACTION_MOVE:
					final int index = TouchUtil.findPoinerIndex(event, this.pointerId);
					if (index == -1){
						return false;
					}

				break;

                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_POINTER_UP:
                    if (pressed){
                        final FireEventFistener eventListener = mFireEventListener;
                        if (eventListener != null){
                            eventListener.onFire();
                        }
					}

					if (this.pointerId == pointerId){
						this.pointerId = -1;
					}else{
						return false;
					}

                case MotionEvent.ACTION_CANCEL:
                    pressed = false;
            }






        return false;
    }
}
