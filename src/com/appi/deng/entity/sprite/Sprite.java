package com.appi.deng.entity.sprite;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import com.appi.deng.Render;
import com.appi.deng.entity.Entity;
import com.appi.deng.entity.connectors.CollisionConnector;
import com.appi.deng.entity.layer.Layer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 12/17/13
 * Time: 10:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class Sprite extends Entity{


	public static class Collision{
		public boolean isCollide;
		public Sprite spriteA;
		public Sprite spriteB;
		public Emitter.Emission emissionA;
		public Emitter.Emission emissionB;
	}

	private PointF mScale;

    public float x = 0;
    public float y = 0;
    public float width;
    public float height;
    public float angle;
	protected int halfWidth;
	protected int halfHeight;

    private Drawable mDrawable;

    public Sprite(Drawable drawable){
        mDrawable = drawable;
        width = drawable.getIntrinsicWidth();
        height = drawable.getIntrinsicHeight();

		halfWidth = (int)(width / 2);
		halfHeight = (int)(height / 2);
    }

	@Override
	public void onAttached(Render render) {
		super.onAttached(render);
		mScale = render.getScale();
	}

	@Override
    public void draw(Canvas canvas, float tpf) {
		if (! visible) return;
        canvas.save();

        canvas.translate(x , y );
        float a = (float)((angle * 180) / Math.PI);
        canvas.rotate(a, 0 , 0);
        mDrawable.setBounds(-halfWidth, -halfHeight, halfWidth, halfHeight);
        mDrawable.draw(canvas);

        postDraw(canvas, tpf);

        canvas.restore();
    }

    protected void postDraw(Canvas canvas, float tpf){
    }



	public void isCollide(Sprite sprite, Collision collision){
		collision.isCollide = isCollide(this.x - halfWidth, this. y - halfHeight, this.x + halfWidth, this.y + halfHeight,
				sprite.x - sprite.halfWidth , sprite. y - sprite.halfHeight, sprite.x + sprite.halfWidth, sprite.y + sprite.halfHeight);
	}


	public static final boolean isCollide(float leftA, float topA, float rightA, float bottomA, float leftB, float topB, float rightB, float bottomB){
		return !( rightA < leftB ||
				  bottomA < topB ||
				  rightB < leftA ||
				  bottomB < topA);
	}


}
