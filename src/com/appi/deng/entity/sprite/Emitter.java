package com.appi.deng.entity.sprite;

import android.graphics.Canvas;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 5/13/14
 * Time: 4:24 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Emitter extends Sprite{

	private static final int DESAPIAR_DISTANCE = 300;

	public static class Emission {

		/*package*/ Emission(){
		}

		public float x;
		public float y;
		public float xSh;
		public float ySh;
		public boolean destroy;
	}


	private static final Queue<Emission> mRecycledEmitions = new LinkedList<Emission>();

	private final List<Emission> mEmissions = new ArrayList<Emission>(20);
	private final List<Emission> mEmittionsToRemove = new ArrayList<Emission>(20);

	private final Drawable mEmittionDrawable;
	private final int mEmittionWidth;
	private final int mEmittionHeight;

	private float mEmittionFreaquency = 1;

	private float mStartEmittedTime;
	private volatile boolean isEmitted = false;
	private volatile boolean mStartEmittion = false;

	protected float mMoveSpeed = 100;


	public Emitter(Drawable prototypeDrawable) {
		super(new ColorDrawable(0x00000000));
		mEmittionDrawable = prototypeDrawable;
		mEmittionWidth = prototypeDrawable.getIntrinsicWidth();
		mEmittionHeight = prototypeDrawable.getIntrinsicHeight();
	}


	private Emission getEmmittion(){
		Emission bullet;
		synchronized (mRecycledEmitions){
			if (mRecycledEmitions.isEmpty()){
				bullet = new Emission();

			}else{
				bullet = mRecycledEmitions.poll();
			}
			bullet.destroy = false;
			bullet.x = 0;
			bullet.y = 0;
			bullet.xSh = 0;
			bullet.ySh = 0;
		}
		return bullet;
	}

	public void setEmittionFrequency(float emittionFrequency){
		mEmittionFreaquency = 1/emittionFrequency;
	}

	public void startEmittion(boolean now){
		if (now){
			mStartEmittedTime = mEmittionFreaquency+1f;
		}
		mStartEmittion = true;
		isEmitted = true;
	}

	public void stopEmittion(){
		mStartEmittion = false;

	}


//	@Override
//	public void isCollide(float left, float top, float right, float bottom, Collision collision) {
//		for (Emission emission : mEmissions){
//			if (emission.destroy) continue;
//
//			final boolean isCollide = Sprite.isCollide(left, top, right, bottom, emission.x, emission.y, emission.x + mEmittionWidth, emission.y + mEmittionHeight);
//			if (isCollide){
//				collision.isCollide = true;
//			}
//
//		}
//	}


	@Override
	public void isCollide(Sprite sprite, Collision collision) {

		if (sprite instanceof Emitter){
			final Emitter emitter = (Emitter)sprite;
			for (Emission emissionA : mEmissions){
				for (Emission emissionB : emitter.mEmissions){
					final boolean isCollide = Sprite.isCollide(emissionA.x, emissionA.y, emissionA.x + mEmittionWidth, emissionA.y + mEmittionHeight,
							emissionB.x, emissionB.y, emissionB.x + emitter.mEmittionWidth, emissionB.y + emitter.mEmittionHeight);
					if (isCollide){
						collision.isCollide = true;
						collision.emissionA = emissionA;
						collision.emissionB = emissionB;
						return;
					}
				}
			}

		}else{

			for (Emission emission : mEmissions){
				if (emission.destroy) continue;

				final boolean isCollide = Sprite.isCollide(sprite.x - sprite.halfWidth, sprite.y - sprite.halfHeight, sprite.x + sprite.halfWidth, sprite.y + sprite.halfHeight,
						emission.x, emission.y, emission.x + mEmittionWidth, emission.y + mEmittionHeight);
				if (isCollide){
					collision.isCollide = true;
					collision.emissionA = emission;
					return;
				}

			}
		}
	}

	@Override
	public void draw(Canvas canvas, float tpf) {
		if (!mStartEmittion && !isEmitted) return;


		if (mStartEmittion){
			mStartEmittedTime += tpf;
			if (createNewEmittion(mStartEmittedTime)){
				mStartEmittedTime = 0;
				final Emission emission = getEmmittion();
				initEmittion(emission, getViewPort());

				mEmissions.add(emission);
			}
		}


		mEmittionsToRemove.clear();
		for (Emission emission : mEmissions){

			produceMove(emission, tpf);

			final int x = (int) emission.x;
			final int y = (int) emission.y;
			final int width = x + mEmittionWidth;
			final int height = y + mEmittionHeight;
			mEmittionDrawable.setBounds(x, y, width, height);
			mEmittionDrawable.draw(canvas);

			final RectF viewPort = getViewPort();

			if (emission.destroy){ // if this particle marks as destroyed, add to remove queue
				mEmittionsToRemove.add(emission);
			}else { // else check bounds.
				if (emission.x > viewPort.right +DESAPIAR_DISTANCE ||
						emission.y > viewPort.bottom +DESAPIAR_DISTANCE ||
						emission.x < viewPort.left - DESAPIAR_DISTANCE ||
						emission.y < viewPort.top - DESAPIAR_DISTANCE){
					mEmittionsToRemove.add(emission);
				}
			}

		}

		if(!mEmittionsToRemove.isEmpty()){
			mEmissions.removeAll(mEmittionsToRemove);
			mRecycledEmitions.addAll(mEmittionsToRemove);
		}

		if (!mStartEmittion && mEmissions.isEmpty()){
			isEmitted = false;
		}
	}


	public void setEmittionSpeed(float speed){
		mMoveSpeed = speed;
	}

	protected abstract void initEmittion(Emission emission, RectF viewPort);

	protected void produceMove(Emission emission, float tpf){
		emission.x += emission.xSh * tpf;
		emission.y += emission.ySh * tpf;
	}

	protected boolean createNewEmittion(float timeFromLastEmittion){
		return mStartEmittedTime > mEmittionFreaquency;
	}




}
