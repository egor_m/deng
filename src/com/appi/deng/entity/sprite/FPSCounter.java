package com.appi.deng.entity.sprite;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import com.appi.deng.entity.Entity;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 12/17/13
 * Time: 11:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class FPSCounter extends Entity {

    private final Paint mBgPaint = new Paint();
    private final Paint mText = new Paint();

    private final float WIDTH = 100;
    private final float HEIGHT = 20;

    private final float FONT_SIZE = 15;

    private int mFramesCount = 0;
    private float mSpendTime = 0;

    public float fps;

    public FPSCounter(){
        mBgPaint.setColor(Color.BLUE);
        mBgPaint.setStyle(Paint.Style.FILL);

        mText.setColor(Color.WHITE);
        mText.setAntiAlias(true);
        mText.setTextSize(FONT_SIZE);
    }

    @Override
    public void draw(Canvas canvas, float tpf) {

        mSpendTime += tpf;
        mFramesCount ++;

        if (mSpendTime > 0){
            fps = mFramesCount / mSpendTime ;
        }

        if (mSpendTime > 5){
            mSpendTime = 0;
            mFramesCount = 0;
        }


        final float height = getViewPort().height();
        final float width = getViewPort().width();

        canvas.drawRect(width - WIDTH, height - HEIGHT, width, height, mBgPaint);
        canvas.drawText(String.valueOf(fps), width - WIDTH+ FONT_SIZE, height - HEIGHT + FONT_SIZE , mText);
    }
}
