package com.appi.deng.entity.sprite;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 5/14/14
 * Time: 4:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class TextSprite extends Sprite{

	private final Paint mTextPaint = new Paint();
	private String mText;



	public TextSprite() {
		super(new ColorDrawable(0x00000000));
		mTextPaint.setAntiAlias(true);
	}

	public void setTextSize(float textSize){
		mTextPaint.setTextSize(textSize);
	}

	public void setTextColor(int textColor){
		mTextPaint.setColor(textColor);
	}

	public void setText(String text){
		mText = text;
	}


	@Override
	public void draw(Canvas canvas, float tpf) {
		if (mText != null){
			canvas.drawText(mText, x, y, mTextPaint);
		}
	}
}
