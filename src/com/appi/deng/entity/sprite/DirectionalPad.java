package com.appi.deng.entity.sprite;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.view.MotionEvent;
import com.appi.deng.TouchEventConverter;
import com.appi.deng.entity.Entity;
import com.appi.deng.entity.Touchable;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 12/18/13
 * Time: 12:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class DirectionalPad extends Entity implements Touchable{


    public interface DirectionChangedListener{
        public void onDirectionChanged(float angle, float froce, float forceX, float forceY);
    }

    public float x = 0;
    public float y = 0;
    public float radius = 150;

    private float cursorX = 0;
    private float cursorY = 0;
    private float cursorRadius = 50;
    private boolean showCursor = false;

	private float padding = 10f;


    private final Paint mPaint;
    private final Paint mCursorPaint;

    private DirectionChangedListener mDirectionChangedListener;

    public DirectionalPad(){
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(5);
        mPaint.setColor(Color.RED);


        mCursorPaint = new Paint();
        mCursorPaint.setAntiAlias(true);
        mCursorPaint.setColor(Color.BLUE);
    }


    public void setDirectionChangeListener(DirectionChangedListener directionChangeListener){
        mDirectionChangedListener = directionChangeListener;
    }

    @Override
    public void draw(Canvas canvas, float tfp) {

        x = radius + padding;
        y = getViewPort().bottom - radius - padding;

        canvas.save();

        canvas.translate(x, y);

        canvas.drawCircle(0,0, radius, mPaint);

        if (showCursor){
            canvas.drawCircle(cursorX, cursorY, cursorRadius, mCursorPaint);
        }

        canvas.restore();
    }

    private int pointerId = -1;

    @Override
    public boolean onTouchEvent(MotionEvent event, TouchEventConverter converter) {

        final int action = event.getActionMasked();

        int pointerIndex = event.getActionIndex();
        int pointerId = event.getPointerId(pointerIndex);


        float x = converter.convertX(event.getX(pointerIndex)) -  this.x; //translate touches
        float y = converter.convertY(event.getY(pointerIndex)) - this. y;



        switch (action){
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:

				if (x < -(radius+padding) || x > +(radius+padding) ||
					y < -(radius + padding) || y > +(radius+padding)){
					return  false;
				}

                showCursor = true;
                if(this.pointerId == -1){
                    this.pointerId = pointerId;
                } else{
					if (this.pointerId != pointerId) return false;
				}
            case MotionEvent.ACTION_MOVE:

			    final int index = TouchUtil.findPoinerIndex(event, this.pointerId);
				if (index == -1) return false;

				x = converter.convertX(event.getX(index)) -  this.x; //translate touches
				y = converter.convertY(event.getY(index)) - this. y;

                cursorX = x;
                cursorY = y;

                final DirectionChangedListener directionChangedListener = mDirectionChangedListener;
                if (directionChangedListener != null){
					final float angle = (float)( Math.atan2(y , x) + Math.PI /2 );
                    float power = (float)(Math.sqrt(x*x + y* y) / radius);
					float forceX = 0;
					float forceY = 0;
					if (Math.abs(x) > cursorRadius){
						forceX = 1 * Math.signum(x);
					}else{
						forceX = x / cursorRadius;
					}
					if (Math.abs(y) > cursorRadius){
						forceY = 1 * Math.signum(y);
					}else{
						forceY = y / cursorRadius;
					}
                    if (power > 1) power = 1;
					notifyDirectionChanged(directionChangedListener, angle, power, forceX, forceY);
                }


            break;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
            case MotionEvent.ACTION_CANCEL:
                showCursor = false;

                final DirectionChangedListener dl = mDirectionChangedListener;
                final float a = (float)( Math.atan2(y , x) + Math.PI /2 );
                if (dl != null){
					notifyDirectionChanged(dl, a, 0f, 0f, 0f);
                }

				if (this.pointerId == pointerId){
                	this.pointerId = -1;
				}else{
					return false;
				}

            break;

        }

        return true;
    }





	protected void notifyDirectionChanged(DirectionChangedListener directionChangedListener, float angle, float power, float forceX, float forceY){
		directionChangedListener.onDirectionChanged(angle, power, forceX, forceY);
	}
}
