package com.appi.deng.entity.sprite;

import android.graphics.Canvas;
import android.graphics.RectF;
import android.view.MotionEvent;
import com.appi.deng.TouchEventConverter;
import com.appi.deng.entity.Entity;
import com.appi.deng.entity.Touchable;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 7/12/14
 * Time: 5:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class ClickPad extends Entity implements Touchable{


	public interface ClickPadListener{
		public void onClicked(float x, float y);
	}


	private boolean pressed = false;
	private int pointerId = -1;
	private ClickPadListener mClickPadListener;

	public void setClickListener(ClickPadListener clickListener){
		mClickPadListener = clickListener;
	}

	@Override
	public void draw(Canvas canvas, float tfp) {
	}

	@Override
	public boolean onTouchEvent(MotionEvent event, TouchEventConverter converter) {
		final RectF viewPort = getViewPort();

		final int action = event.getActionMasked();

		int pointerIndex = event.getActionIndex();
		int pointerId = event.getPointerId(pointerIndex);


		final float x = converter.convertX(event.getX(pointerIndex));
		final float y = converter.convertY(event.getY(pointerIndex));




			switch (action){
				case MotionEvent.ACTION_DOWN:
				case MotionEvent.ACTION_POINTER_DOWN:
					pressed = true;
					if(this.pointerId == -1){
						this.pointerId = pointerId;
					} else{
						if (this.pointerId != pointerId) return false;
					}
					break;

				case MotionEvent.ACTION_UP:
				case MotionEvent.ACTION_POINTER_UP:
					if (pressed){

						if (this.pointerId == pointerId){
							this.pointerId = -1;
						}else{
							return false;
						}

						final ClickPadListener clickPadListener = mClickPadListener;
						if (clickPadListener != null){
							clickPadListener.onClicked(x, y);
						}

					}



				case MotionEvent.ACTION_CANCEL:
					pressed = false;
			}

			return false;




	}
}
