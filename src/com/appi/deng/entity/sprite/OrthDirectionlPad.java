package com.appi.deng.entity.sprite;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 4/18/14
 * Time: 2:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class OrthDirectionlPad extends DirectionalPad{



	protected void notifyDirectionChanged(DirectionChangedListener directionChangedListener, float angle, float power){


		System.out.println(angle);


		if ( (angle >= (7*Math.PI)/4 && angle <= 2*Math.PI) ||(angle >= 0 && angle < Math.PI/4)){ // 0 deg sector
			angle = 0;
		}else
		if (angle >= Math.PI/4 && angle < (3*Math.PI)/4){ // 90 deg sector
			angle = (float)Math.PI/2;
		}else
		if (angle >= (3*Math.PI)/4 && angle < (5*Math.PI)/4){ // 180 deg sector
			angle = (float)Math.PI;
		}else
		if (angle >= (5*Math.PI)/4 && angle < (7*Math.PI)/4){ // 270 deg sector
			angle = (float)(3*Math.PI/2);
		}




		directionChangedListener.onDirectionChanged(angle, power, 0, 0);
	}

}
