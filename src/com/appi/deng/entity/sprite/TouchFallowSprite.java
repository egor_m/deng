package com.appi.deng.entity.sprite;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import com.appi.deng.TouchEventConverter;
import com.appi.deng.entity.Touchable;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 5/8/14
 * Time: 11:46 AM
 * To change this template use File | Settings | File Templates.
 */
public class TouchFallowSprite extends Sprite implements Touchable{

	private float mTargetX;
	private float mTargetY;

	private float mMaxSpeed = 100f;
	private boolean isTouchInProcess = false;

	public TouchFallowSprite(Drawable drawable) {
		super(drawable);
	}

	public void setMaxSpeed(float speed){
		mMaxSpeed = speed;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event, TouchEventConverter converter) {

		mTargetX = converter.convertX(event.getX()); //translate touches
		mTargetY = converter.convertY(event.getY());


		switch (event.getAction()){
			case MotionEvent.ACTION_DOWN:
				isTouchInProcess = true;
			break;
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_UP:
				isTouchInProcess = false;
			break;
		}


		return true;
	}


	@Override
	public void draw(Canvas canvas, float tpf) {

		if (isTouchInProcess){

			final float xSh = x - mTargetX;
			final float ySh = y - mTargetY;
			final float xShSig = Math.signum(xSh);
			final float yShSig = Math.signum(ySh);

			final float speed = mMaxSpeed * tpf;

			x -= xShSig * Math.min(Math.abs(xSh), speed);
			y -= yShSig * Math.min(Math.abs(ySh), speed);
		}



		super.draw(canvas, tpf);
	}
}
