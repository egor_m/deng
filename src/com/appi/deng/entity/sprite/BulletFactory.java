package com.appi.deng.entity.sprite;

import android.graphics.drawable.Drawable;
import com.appi.deng.entity.layer.Layer;
import com.appi.deng.physics.BulletBody;
import com.appi.deng.physics.CollideDetectedListener;
import com.appi.shpx.World;
import com.appi.shpx.body.Body;

import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 7/10/14
 * Time: 11:14 AM
 * To change this template use File | Settings | File Templates.
 */
public class BulletFactory {

	private final LinkedList<BulletBody> mBullets = new LinkedList<BulletBody>();


	private BulletFactory(){
	}

	public static BulletFactory createFactory(int bulletsCount, Drawable bulletDrawable, CollideDetectedListener collideDetectedListener){

		final BulletFactory bulletFactory = new BulletFactory();

		for (int i = 0 ; i < bulletsCount; i++){

			final BulletSprite bulletSprite = new BulletSprite(bulletDrawable);
			bulletSprite.visible = false;

			final BulletBody bulletBody = new BulletBody(bulletSprite, bulletFactory);
			bulletBody.registerCollideDetector(collideDetectedListener);

			bulletFactory.mBullets.add(bulletBody);
		}

		return bulletFactory;

	}


	public synchronized BulletBody getFreeBullet(long collisionGroup){
		if (mBullets.isEmpty()) return null;
		final BulletBody bulletBody = mBullets.pop();
		bulletBody.collisionGroup = collisionGroup;
		return bulletBody;
	}



	public synchronized void recycle(BulletBody bulletBody){
		bulletBody.clear();


		mBullets.add(bulletBody);
	}


	public void init(World world, Layer layer){
		for (BulletBody body : mBullets){
			world.addBody(body);
			layer.addEntity(body.getBulletSprite());
		}
	}

}
