package com.appi.deng.entity.sprite;

import android.graphics.RectF;
import android.graphics.drawable.Drawable;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 5/13/14
 * Time: 1:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class EnemyEmitter extends Emitter{

	private final Random mRandom = new Random();

	public EnemyEmitter(Drawable prototypeDrawable) {
		super(prototypeDrawable);
	}

	@Override
	protected void initEmittion(Emission emission, RectF viewPort) {

		emission.x =  viewPort.right+200;
		emission.y = mRandom.nextInt((int)viewPort.bottom);
		emission.xSh = -mMoveSpeed;
		emission.ySh = 0;
	}



}
