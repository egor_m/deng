package com.appi.deng.entity.sprite;

import android.graphics.RectF;
import android.graphics.drawable.Drawable;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 5/13/14
 * Time: 11:24 AM
 * To change this template use File | Settings | File Templates.
 */
public class BulletsEmitter extends Emitter {


	public BulletsEmitter(Drawable prototypeDrawable) {
		super(prototypeDrawable);
	}

	@Override
	protected void initEmittion(Emission emission, RectF viewPort) {
		emission.x = this.x;
		emission.y = this.y;
		emission.xSh = mMoveSpeed;
		emission.ySh = 0;
	}


}
