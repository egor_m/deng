package com.appi.deng.entity.sprite;

import android.graphics.drawable.Drawable;
import com.appi.deng.entity.animation.AnimatedSprite;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 10/22/14
 * Time: 4:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class GrenadeSprite extends Sprite implements AnimatedSprite.Callback{

	private final float hW;
	private final float hH;

	private final AnimatedSprite mGrenateExplosionAnimation;


	public GrenadeSprite(Drawable drawable, Drawable grenateExplosionAnimation) {
		super(drawable);
		hW = halfWidth;
		hH = halfHeight;

		mGrenateExplosionAnimation = new AnimatedSprite(grenateExplosionAnimation);
	}

	public void clear() {
		visible = false;
	}

	public void scale(float k){
		halfWidth = (int)(hW * k);
		halfHeight = (int)(hH * k);
	}

	public void explode() {
		mGrenateExplosionAnimation.x = x;
		mGrenateExplosionAnimation.y = y;
		mGrenateExplosionAnimation.start();
	}

	@Override
	public void onAnimationStart(AnimatedSprite animatedSprite) {
	}

	@Override
	public void onAnimationFinished(AnimatedSprite animatedSprite) {
	}
}
