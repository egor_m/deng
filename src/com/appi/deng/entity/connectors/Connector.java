package com.appi.deng.entity.connectors;

import android.graphics.Canvas;
import com.appi.deng.entity.Entity;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 5/12/14
 * Time: 3:29 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Connector extends Entity{



	@Override
	public void draw(Canvas canvas, float tfp) {
		step(tfp);
	}



	protected abstract void step(float tfp);

}
