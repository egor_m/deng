package com.appi.deng.entity.connectors;

import com.appi.deng.entity.sprite.Sprite;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 5/12/14
 * Time: 3:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class FixedConnector extends Connector {


	public static enum Anchor{
		LEFT,TOP,RIGHT,BOTTOM
	}

	private final Sprite mFixtureSprite;
	private final Sprite mFollowerSprite;
	private final Anchor mAnchor;
	private float mAnchorXShift;
	private float mAnchorYShift;


	public FixedConnector(Sprite fixture, Sprite follower, Anchor anchor){
		mFixtureSprite = fixture;
		mFollowerSprite = follower;
		mAnchor = anchor;
	}

	public void setAnchorShift(float anchorXShift, float anchorYShift){
		mAnchorXShift = anchorXShift;
		mAnchorYShift = anchorYShift;
	}

	@Override
	protected void step(float tfp) {
		switch (mAnchor){
			case LEFT:
				mFollowerSprite.x = mFixtureSprite.x - mFixtureSprite.width/2 + mAnchorXShift;
				mFollowerSprite.y = mFixtureSprite.y                          + mAnchorYShift;
			break;
			case RIGHT:
				mFollowerSprite.x = mFixtureSprite.x + mFixtureSprite.width/2 + mAnchorXShift;
				mFollowerSprite.y = mFixtureSprite.y						  + mAnchorYShift;
			break;
			case TOP:
				mFollowerSprite.x = mFixtureSprite.x						   + mAnchorXShift;
				mFollowerSprite.y = mFixtureSprite.y - mFixtureSprite.height/2 + mAnchorYShift;
			break;
			case BOTTOM:
				mFollowerSprite.x = mFixtureSprite.x						   + mAnchorXShift;
				mFollowerSprite.y = mFixtureSprite.y + mFixtureSprite.height/2 + mAnchorYShift;
			break;
		}

	}
}
