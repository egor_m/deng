package com.appi.deng.entity.connectors;

import android.graphics.Canvas;
import com.appi.deng.entity.Entity;
import com.appi.deng.entity.sprite.Emitter;
import com.appi.deng.entity.sprite.Sprite;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 5/13/14
 * Time: 4:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class CollisionConnector extends Entity{


	private final Sprite.Collision mCollision = new Sprite.Collision();


	public static interface CollisionListener{
		public void onCollide(Sprite spriteA, Sprite spriteB, Emitter.Emission emissionA, Emitter.Emission emissionB);
	}


	private CollisionListener mCollisionListener;
	private final Sprite mSpriteA;
	private final Sprite mSpriteB;

	public CollisionConnector(Sprite spriteA, Sprite spriteB){

		if ( (!(spriteA instanceof Emitter)) && (spriteB instanceof Emitter)){ // Emitter shoud be first
			this.mSpriteA = spriteB;
			this.mSpriteB = spriteA;
		}else{

			this.mSpriteA = spriteA;
			this.mSpriteB = spriteB;
		}
	}


	public void setCollisionListener(CollisionListener collisionListener){
		mCollisionListener = collisionListener;
	}

	@Override
	public void draw(Canvas canvas, float tfp){
		mCollision.isCollide = false;
		mCollision.emissionA = null;
		mCollision.emissionB = null;
		mCollision.spriteA = mSpriteA;
		mCollision.spriteB = mSpriteB;
		mSpriteA.isCollide(mSpriteB, mCollision);
		if (mCollision.isCollide){
			notifyCollide(mCollision);
		}
	}

	private void notifyCollide(Sprite.Collision collision) {
		if (mCollisionListener == null) return;
		mCollisionListener.onCollide(mSpriteA, mSpriteB, collision.emissionA, collision.emissionB);
	}
}
