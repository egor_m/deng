package com.appi.deng.entity;

import android.view.MotionEvent;
import com.appi.deng.TouchEventConverter;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 12/18/13
 * Time: 12:03 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Touchable {

    public boolean onTouchEvent(MotionEvent event, TouchEventConverter converter);
}
