package com.appi.deng.entity;

import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.RectF;
import com.appi.deng.Render;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 12/17/13
 * Time: 9:56 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Entity {

    private static final RectF mEmptyViewPort = new RectF();
	private static final PointF mEmptyScale = new PointF(1f, 1f);

    private Render mRender;
    public Object userPayload;
	public boolean visible = true;

    public abstract void draw(Canvas canvas, float tfp);

    public void onAttached(Render render){
        mRender = render;
    }

    public void onDetached(){
        mRender = null;
    }

    public RectF getViewPort(){
        final Render render = mRender;
        if (render != null){
            return render.getViewPort();
        }else{
            return mEmptyViewPort;
        }
    }

	public PointF getScale(){
		final Render render = mRender;
		if (render != null){
			return render.getScale();
		}else{
			return mEmptyScale;
		}
	}

}
