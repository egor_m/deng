package com.appi.deng.entity.tilemap;

import android.R;
import android.content.Context;
import android.graphics.*;
import android.graphics.drawable.BitmapDrawable;
import com.appi.deng.Render;
import com.appi.deng.entity.layer.Layer;
import com.appi.shpx.level.*;
import com.appi.deng.entity.Entity;
import com.appi.deng.entity.layer.PhysicsLayer;
import com.appi.deng.entity.sprite.Sprite;


import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 12/17/13
 * Time: 10:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class TileMap extends Layer{

	private static final float FOLLOW_K = 0.5f;

    private final Rect src = new Rect();
    private final Rect dst = new Rect();
    private final Paint mPaint;

    private final Level mLevel;
    private final Context mContext;

    private float x;
    private float y;

    private final int[] mMap;
    private final int width;
    private final int height;
    private final int tileWidth;
    private final int tileHeight;

    private Bitmap mTilesSetBitmap;
    private int tilesInRow;

	private RectF mViewPort = new RectF();

	private Bitmap mBackgroundTileBitmap;
	private BitmapDrawable mBG;


	private boolean mUseBg = false;


    private Sprite mFollowSprite;


    public TileMap(Level level, String tileSet, Context context, int bgTileResId){
        mLevel = level;
        mContext = context;

        mPaint = new Paint();
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setColor(Color.RED);



		mBackgroundTileBitmap = BitmapFactory.decodeResource(context.getResources(), bgTileResId);
		mBG = new BitmapDrawable(mBackgroundTileBitmap);
		mBG.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);


        final LevelObject levelObject = mLevel.getLevelObject();

        final LayerObject layerObject = levelObject.layers[0];
        width = levelObject.width;
        height = levelObject.height;

        tileWidth = levelObject.tilewidth;
        tileHeight = levelObject.tileheight;

        mMap = layerObject.data;


        try {
            mTilesSetBitmap = BitmapFactory.decodeStream(mContext.getAssets().open(tileSet));
        } catch (IOException e) {
            e.printStackTrace();
        }
        tilesInRow = mTilesSetBitmap.getWidth() / tileWidth;

    }

	public float getX(){
		return x;
	}

	public float getY(){
		return y;
	}

	/*package*/ Level getLevel(){
		return mLevel;
	}

	protected List<Entity> getObjects(){
		return getEntityList();
	}


	@Override
	public void onAttached(Render render) {
		super.onAttached(render);
		mViewPort = render.getViewPort();

	}

	@Override
    public void draw(Canvas canvas, float tfp, List<Entity> entities) {
        canvas.save();


        final float centerX = getViewPort().centerX();
        final float centerY = getViewPort().centerY();

        final Sprite follow = mFollowSprite;
        if (follow != null){

            final float followSpeed = tfp * FOLLOW_K;

            final float sx = (centerX - follow.x) * followSpeed;
            final float sy = (centerY - follow.y) * followSpeed;
            x = centerX - follow.x;
            y = centerY - follow.y ;

//			x = sx;
//			y =sy;
        }




		canvas.translate(x, y);


		if (mUseBg){
			mBG.setBounds((int)-x,(int)-y, (int)(mViewPort.right-x), (int)(mViewPort.bottom-y));
			mBG.draw(canvas);
		}


		int k = 0;
        for (int i = 0 ; i < width ; i++){
            for (int j = 0 ; j < height ; j++){
                final int tile = mMap[k];
                drawTile(canvas, tile, j * tileWidth, i * tileHeight);
                k++;
            }
        }

        for (Entity entity : entities){
			if (! entity.visible) continue;
            entity.draw(canvas, tfp);
        }

        canvas.restore();

    }


    /*package*/ void drawTile(Canvas canvas, int tile, int x, int y){
        if (tile == 0) return;

		tile -=1;

        final int row = tile / tilesInRow;
        final int col = tile % tilesInRow;

        src.left = col * tileWidth;
        src.top = row * tileHeight;
        src.right = tileWidth + src.left;
        src.bottom = tileHeight + src. top;

        dst.left = x;
        dst.top = y;
        dst.right = x + tileWidth;
        dst.bottom = y + tileHeight;


        canvas.drawBitmap(mTilesSetBitmap, src, dst, mPaint);
    }

    public void setFollowEntity(Sprite followSprite){
        mFollowSprite = followSprite;
    }


}
