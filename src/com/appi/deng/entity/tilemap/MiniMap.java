package com.appi.deng.entity.tilemap;

import android.graphics.*;
import android.graphics.drawable.Drawable;
import com.appi.deng.Render;
import com.appi.deng.entity.Entity;
import com.appi.deng.entity.sprite.Sprite;
import com.appi.deng.entity.tilemap.TileMap;
import com.appi.deng.physics.TactileSprite;
import com.appi.shpx.level.LayerObject;
import com.appi.shpx.level.Level;
import com.appi.shpx.level.LevelObject;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 7/10/14
 * Time: 10:54 AM
 * To change this template use File | Settings | File Templates.
 */
public class MiniMap extends Entity{

	private final int ALPHA = 150;

	private final TileMap mTileMap;
	private final int mWidth;
	private final int mHeight;
	private final Bitmap mMapBitmap;
	private final Paint mPaint;
	private final List<Entity> mEntities;

	private float mScaleX;
	private float mScaleY;


	public MiniMap(TileMap tileMap, int width, int height){
		mTileMap = tileMap;
		mWidth = width;
		mHeight = height;

		mEntities = mTileMap.getObjects();

		mMapBitmap =  Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);

		mPaint = new Paint();
		mPaint.setColor(Color.WHITE);
		mPaint.setAntiAlias(true);


		prepareMapBitmap();
	}


	private void prepareMapBitmap(){
		final Level level = mTileMap.getLevel();
		final LevelObject levelObject = level.getLevelObject();
		final LayerObject layerObject = levelObject.getFirstLayerObject();

		int[] map = layerObject.data;

		final float width = layerObject.width * levelObject.tilewidth;
		final float height = layerObject.height * levelObject.tileheight;

		final float scaleX = mWidth / width;
		final float scaleY = mHeight / height;

		mScaleX = scaleX;
		mScaleY = scaleY;

		final Canvas canvas = new Canvas(mMapBitmap);
		canvas.save();
		canvas.scale(scaleX, scaleY);
		mPaint.setColor(Color.WHITE);
		mPaint.setAlpha(ALPHA);
		canvas.drawPaint(mPaint);

		int k = 0;
		for (int i = 0 ; i < layerObject.width ; i++){
			for (int j = 0 ; j < layerObject.height ; j++){
				final int tile = map[k];
				mTileMap.drawTile(canvas, tile, j * levelObject.tilewidth, i * levelObject.tileheight);
				k++;
			}
		}

		canvas.restore();


	}





	@Override
	public void onAttached(Render render) {
		super.onAttached(render);
	}

	@Override
	public void draw(Canvas canvas, float tfp) {
		canvas.drawBitmap(mMapBitmap, 0,0, mPaint);
		mPaint.setColor(Color.GREEN);
		mPaint.setAlpha(ALPHA);

		for (Entity entity : mEntities){
			if (entity instanceof Sprite){
				final Sprite sprite = (Sprite)entity;
				final float x = sprite.x * mScaleX;
				final float y = sprite.y  *mScaleY;
				canvas.drawCircle(x,y, 5, mPaint);
			}
		}

	}
}
