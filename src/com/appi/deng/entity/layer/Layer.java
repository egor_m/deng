package com.appi.deng.entity.layer;

import android.graphics.Canvas;
import com.appi.deng.Render;
import com.appi.deng.entity.Entity;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 12/30/13
 * Time: 4:37 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Layer extends Entity{


    private final List<Entity> mEntities;
	private Render mRender;

    public Layer(){
        mEntities = new CopyOnWriteArrayList<Entity>();
    }

	@Override
	public void onAttached(Render render) {
		super.onAttached(render);
		mRender = render;
	}

	@Override
	public void onDetached() {
		super.onDetached();
		mRender = null;
	}

	public void addEntity(Entity entity){
        mEntities.add(entity);
		final Render render = mRender;
		if (render != null){
			entity.onAttached(render);
		}
    }

    public void removeEntity(Entity entity){
        mEntities.remove(entity);
		entity.onDetached();
    }

    @Override
    public void draw(Canvas canvas, float tfp) {
        draw(canvas, tfp, mEntities);
    }

	protected List<Entity> getEntityList(){
		return mEntities;
	}

    public abstract void draw(Canvas canvas, float tfp, List<Entity> entities);
}
