package com.appi.deng.entity.layer;

import android.graphics.Canvas;
import com.appi.deng.entity.Entity;
import com.appi.deng.entity.sprite.Sprite;
import com.appi.deng.physics.Tactile;
import com.appi.shpx.World;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 12/30/13
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class PhysicsLayer extends Layer{

	private final World mWorld;


	public PhysicsLayer(World world){
		mWorld = world;
	}


	@Override
	public void draw(Canvas canvas, float tfp) {
		mWorld.step(tfp);
	}

	@Override
	public void draw(Canvas canvas, float tfp, List<Entity> entities) {

	}
}
