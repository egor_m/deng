package com.appi.deng.entity.layer;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import com.appi.deng.entity.Entity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 5/8/14
 * Time: 11:09 AM
 * To change this template use File | Settings | File Templates.
 */
public class StaticBackgroundLayer extends Layer{

	public static enum ScaleType{
		FILL_XY,
		FILL_SAVE_RATIOS

	}

	private final Drawable mBackgroudDrawable;
	private final ScaleType mScaleType;

	private int mX;
	private int mY;
	private int mWidth;
	private int mHeight;

	private int mSavedCanvasWidth = 0;
	private int mSavedCanvasHeight = 0;



	public StaticBackgroundLayer(Drawable background, ScaleType scaleType){
		mBackgroudDrawable = background;
		mScaleType = scaleType;
	}


	@Override
	public void draw(Canvas canvas, float tfp, List<Entity> entities) {
		final int canvasWidth = canvas.getWidth();
		final int canvasHeight = canvas.getHeight();

		if (mSavedCanvasHeight != canvasHeight || mSavedCanvasWidth != canvasWidth){
			calculateBackgroundDimens(canvasWidth, canvasHeight);
		}

		mBackgroudDrawable.setBounds(mX, mY, mWidth, mHeight);
		mBackgroudDrawable.draw(canvas);

	}


	private void calculateBackgroundDimens(int canvasWidth, int canvasHeight){

		final int width = mBackgroudDrawable.getIntrinsicWidth();
		final int height = mBackgroudDrawable.getMinimumHeight();

		switch (mScaleType){
			case FILL_XY:
				mX = 0;
				mY = 0;
				mWidth = canvasWidth;
				mHeight = canvasHeight;
			break;
			case FILL_SAVE_RATIOS:
				//final float ratios = canvasWidth / (canvasHeight + 0f);
				final float xRatios = canvasWidth / (width + 0f);
				final float yRatios = canvasHeight / (height + 0f);
				final float maxRatios = Math.max(xRatios, yRatios);


				final int newWidth = (int)(width * maxRatios);
				final int newHeight = (int)(height * maxRatios);

				mX = (canvasWidth - newWidth)/2;
				mY = (canvasHeight - newHeight)/2;

				mWidth = mX + newWidth;
				mHeight = mY + newHeight;


			break;
		}

		mSavedCanvasWidth = canvasWidth;
		mSavedCanvasHeight = canvasHeight;
	}
}
