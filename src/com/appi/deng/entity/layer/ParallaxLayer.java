package com.appi.deng.entity.layer;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import com.appi.deng.entity.Entity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 5/8/14
 * Time: 10:24 AM
 * To change this template use File | Settings | File Templates.
 */
public class ParallaxLayer extends Layer{

	private final Drawable mParallaxBackground;
	private final int mBgWidth;
	private final int mBgHeight;

	private float mX,mY;

	private float mSpeed = 0;

	public ParallaxLayer(Drawable background){
		mParallaxBackground = background;
		mBgWidth = mParallaxBackground.getIntrinsicWidth();
		mBgHeight = mParallaxBackground.getIntrinsicHeight();
	}


	@Override
	public void draw(Canvas canvas, float tfp, List<Entity> entities) {

		mX+=mSpeed*tfp;

		final int x = (int)mX % mBgWidth;
		final int y = (int)mY % mBgWidth;
		final int width = mBgWidth + x;
		final int height = mBgHeight + y;


		mParallaxBackground.setBounds(x - mBgWidth, y, x, height);
		mParallaxBackground.draw(canvas);

		mParallaxBackground.setBounds(x, y, width, height);
		mParallaxBackground.draw(canvas);

		mParallaxBackground.setBounds(width,y, width*2, height);
		mParallaxBackground.draw(canvas);

	}

	public void setSpeed(float speed){
		this.mSpeed = speed;
	}


	public void setY(float y) {
		mY = y;
	}
}
