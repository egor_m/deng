package com.appi.deng.entity.animation;

import android.graphics.Canvas;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.SystemClock;
import android.widget.ImageView;
import com.appi.deng.entity.sprite.Sprite;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 12/25/13
 * Time: 6:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class AnimatedSprite extends Sprite {

    private final Drawable mDrawable;
    private final AnimationCallback mAnimationCallback;
	private Callback mCallback;
    private final static Handler HANDLER = new Handler();
    private volatile boolean mShow;




	public interface Callback{
		public void onAnimationStart(AnimatedSprite animatedSprite);
		public void onAnimationFinished(AnimatedSprite animatedSprite);
	}

    public AnimatedSprite(Drawable drawable) {
        super(drawable);
        mAnimationCallback = new AnimationCallback();
        mDrawable = drawable;
    }

	public void setCallback(Callback callback){
		this.mCallback = callback;
	}

    public void start(){
        mShow = true;
        if (! (mDrawable instanceof AnimationDrawable)) return;

        final AnimationDrawable animationDrawable = (AnimationDrawable)mDrawable;

        animationDrawable.setCallback(mAnimationCallback);

        animationDrawable.run();

		final Callback callback = mCallback;
		if (callback != null){
			callback.onAnimationStart(this);
		}
    }


	public void stop(){
		mShow = false;
	}

	public void setOneShot(boolean oneShot){
		if (! (mDrawable instanceof AnimationDrawable)) return;

		final AnimationDrawable animationDrawable = (AnimationDrawable)mDrawable;
		animationDrawable.setOneShot(oneShot);
	}

    @Override
    public void draw(Canvas canvas, float tpf) {
        if (mShow){
            super.draw(canvas, tpf);
        }
    }

    private class AnimationCallback implements Drawable.Callback{

        @Override
        public void invalidateDrawable(Drawable who) {
            // draw will be render without any nofification
        }

        @Override
        public void scheduleDrawable(Drawable who, Runnable what, long when) {
			if (mShow){
				final long delay = when - SystemClock.uptimeMillis();
				HANDLER.postDelayed(what, delay);
				//TODO (perfomance, gui) // It seems handler is slower, that can be implemented id draw() cycle
			}
        }

        @Override
        public void unscheduleDrawable(Drawable who, Runnable what) {
            HANDLER.removeCallbacks(what);
            mShow = false;

			final Callback callback = mCallback;
			if (callback != null){
				callback.onAnimationFinished(AnimatedSprite.this);
			}
        }
    }
}
