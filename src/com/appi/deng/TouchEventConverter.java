package com.appi.deng;

import android.graphics.PointF;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 12/25/13
 * Time: 5:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class TouchEventConverter {

    private final Render mRender;
	private final PointF mScale;

    /*package*/ TouchEventConverter(Render render){
        mRender = render;
		mScale = mRender.getScale();
    }


    public float convertX(float x){
        return x / mScale.x;
    }

    public float convertY(float y){
        return y / mScale.y;
    }
}
