package com.appi.deng.interpolator;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 10/23/14
 * Time: 3:35 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Interpolator {

	public float interpolate(float pct);
}
