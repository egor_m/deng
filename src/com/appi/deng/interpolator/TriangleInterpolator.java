package com.appi.deng.interpolator;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 10/23/14
 * Time: 3:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class TriangleInterpolator implements Interpolator{


	@Override
	public float interpolate(float pct) {
		if (pct < 0.5f){
			return pct * 2;
		}else{
			return (1-pct)*2;
		}
	}
}
