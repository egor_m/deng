package com.appi.deng;

import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.appi.deng.entity.Entity;
import com.appi.deng.entity.Touchable;
import com.appi.deng.entity.sprite.FPSCounter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created with IntelliJ IDEA.
 * User: egormorozov
 * Date: 12/17/13
 * Time: 9:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class Render extends SurfaceView implements SurfaceHolder.Callback {

    // ================== FPS ========================
    public static final boolean SHOW_FPS = true;

    private FPSCounter mFpsCounter = new FPSCounter();
    // ===============================================

    private static final int INIT_ENTITY_SIZE = 50;


    /*package*/ final ReentrantLock mRenderLock = new ReentrantLock();
    /*package*/ volatile boolean isRenderLoopWork;                           // package-private for cheap performanse only;
    /*package*/ final List<Runnable> mRenderTasks;
    /*package*/ final List<Entity> mLayers;

    private final List<Touchable> mTouchables;

    /*package*/ long mSavedTime = 0;
    /*package*/ final Paint mClearColor;

    private final RectF mViewPort = new RectF();
	private final PointF mScale = new PointF(1f, 1f);


    private final TouchEventConverter mTouchEventConverter;

    public Render(Context context, AttributeSet attrs) {
        super(context, attrs);
        mTouchEventConverter = new TouchEventConverter(this);

        setClickable(true);

        getHolder().addCallback(this);
        mRenderTasks = new ArrayList<Runnable>(INIT_ENTITY_SIZE);
        mLayers = new CopyOnWriteArrayList<Entity>();
        mTouchables = new CopyOnWriteArrayList<Touchable>();

        mClearColor = new Paint();
        mClearColor.setColor(Color.WHITE);

		if (SHOW_FPS){
			mFpsCounter.onAttached(this);
		}
    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        isRenderLoopWork = true;
        surfaceHolder.setFormat(PixelFormat.RGBA_8888);
        final RenderTask renderTask = new RenderTask(surfaceHolder);
        final Thread renderThread = new Thread(renderTask);
        renderThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {

		final float sx = width/(1280f);
		final float sy = height/(720f);

		final float scaleX = Math.min(sx, sy);
		final float scaleY = scaleX;

        mViewPort.right = width / scaleX;
        mViewPort.bottom = height / scaleY;
        mViewPort.left = 0;
        mViewPort.top = 0;

		mScale.x = scaleX;
		mScale.y = scaleY;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        isRenderLoopWork = false;
    }


    public void setClearColor(int color){
        mClearColor.setColor(color);
    }

    private class RenderTask implements Runnable {

        private final SurfaceHolder mHolder;

        public RenderTask(SurfaceHolder surfaceHolder) {
            mHolder = surfaceHolder;
        }

        @Override
        public void run() {
            while (isRenderLoopWork) {

                final long currentTime = System.currentTimeMillis();
                if (mSavedTime == 0) {
                    mSavedTime = currentTime;
                }
                final float diff = (currentTime - mSavedTime) / 1000f;
                mSavedTime = currentTime;

                if (!mRenderTasks.isEmpty()) {
                    mRenderLock.lock();
                    try {
                        for (Runnable task : mRenderTasks) {
                            task.run();
                        }
                        mRenderTasks.clear();
                    } finally {
                        mRenderLock.unlock();
                    }
                }

                final Canvas canvas = mHolder.lockCanvas();
                if (canvas == null) continue;
                canvas.drawPaint(mClearColor);

                canvas.save();


                canvas.scale(mScale.x, mScale.y);

                for (Entity entity : mLayers) {
                    entity.draw(canvas, diff);
                }

                if (SHOW_FPS) {
                    mFpsCounter.draw(canvas, diff);
                }

                canvas.restore();
                mHolder.unlockCanvasAndPost(canvas);
            }
        }
    }

    public void runOnRenderThread(Runnable task) {
        mRenderLock.lock();
        try{
            mRenderTasks.add(task);
        }finally {
            mRenderLock.unlock();
        }
    }


    public void addLayer(final Entity entity) {
        final boolean added = mLayers.add(entity);
        if (added) {
            entity.onAttached(Render.this);
        }

        if (entity instanceof Touchable) {
            addTouchable((Touchable) entity);
        }
    }

    public void removeLayer(final Entity entity) {
        final boolean added = mLayers.remove(entity);
        if (added) {
            entity.onDetached();
        }


        if (entity instanceof Touchable) {
            mTouchables.remove((Touchable) entity);
        }
    }


    public void addTouchable(Touchable touchable) {
        mTouchables.add(touchable);
    }

    public void removeTouchable(Touchable touchable) {
        mTouchables.remove(touchable);
    }

    public RectF getViewPort(){
        return mViewPort;
    }

	public PointF getScale(){
		return mScale;
	}


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final int size = mTouchables.size();
        if (size == 0) return super.onTouchEvent(event);
        for (int i = size - 1; i >= 0; i--) {
            final Touchable touchable = mTouchables.get(i);
            if (touchable != null){
                final boolean processed = touchable.onTouchEvent(event, mTouchEventConverter);
                if (processed) return true;
            }
        }

        return super.onTouchEvent(event);
    }
}


